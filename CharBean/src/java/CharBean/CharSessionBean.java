/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CharBean;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;
import javax.ejb.Stateless;

/**
 *
 * @author Robbie
 */
@Stateless
public class CharSessionBean 
{

      static int sum = 0;
    static int[] count = new int[1000];
    static String str = "";
    static String fileName = "C:/Users/Robbie/Desktop/GCU_Coursework/CST235/CST_235_TextFile_topic1.txt";

   public CharSessionBean()
        {
        }

    public static void readNumOfChars(String fileName)
        {
        try
        {

            FileReader textFileReader = new FileReader(fileName);
            char[] buffer = new char[8096];
            int numberOfCharsRead = textFileReader.read(buffer);

            System.out.println("Number of characters read for the below file is = " + numberOfCharsRead); //output number of chars
            System.out.println();
            
            while (numberOfCharsRead != -1)
            {
                System.out.println(String.valueOf(buffer, 0, numberOfCharsRead));
                numberOfCharsRead = textFileReader.read(buffer);
            }

        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        }

    public static void readNumOfWords(String fileName)
        {
        try
        {
            int i, j;
            i = j = 0;
            Scanner sc = new Scanner(new File(fileName));
            while (sc.hasNext())
            {
                String str = sc.nextLine();
                System.out.println(str);

                if (str == null || str.isEmpty())
                {
                    System.out.println("0");
                }
                StringTokenizer tokens = new StringTokenizer(str); //split string and add to array
                count[i] = tokens.countTokens();
                i++;
            }

            for (int c : count)
            {   // sum words
                j = j + c;

            }
            System.out.println("Word count equals for above file is " + j);
            System.out.println();

            sc.close();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        }

    public static void readSumOfInts(String fileName)
        {
        try
        {
            int x, y;
            x = 0;
            Scanner sc = new Scanner(new File(fileName));
            while (sc.hasNext())
            {
                String str = sc.nextLine();
                //  System.out.println(str);          

                char[] temp = str.toCharArray();  //create char array

                for (char n : temp)
                {
                    if (n <= '9' && n >= '0') // set range
                    {
                        sum += n - '0';   // subract unicode 48 value from char to give int and sum ints
                      //  System.out.println(sum);
                    }
                }              
                

            } System.out.println("The sum of Integers in the file is " + sum);

            sc.close();
        } catch (IOException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        }
    public static void main(String[]args)
        {
            CharSessionBean csb = new CharSessionBean();
            csb.readNumOfChars(fileName);
            csb.readNumOfWords(fileName);
            csb.readSumOfInts(fileName);
        }
}