/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videoejb_gui;
import VideoEnterpriseJavaBean.*;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javax.naming.*;
import java.util.Properties;
import javafx.geometry.Pos;

/**
 *
 * @author Robbie
 */
public class VideoEJB_GUI extends Application
{
      Label labe = new Label();
    
    @Override
    public void start(Stage primaryStage)
        {
        VideoEJB vid = new VideoEJB();        
        
        
          TextField tf1 = new TextField();
          tf1.setPromptText("Enter Quality 1080 or 720");
          tf1.setPrefColumnCount(25);
          
          tf1.setOnAction(new EventHandler<ActionEvent>()
          {
          @Override
          public void handle(ActionEvent ae)
           {
              
           }
          
          } );
          
          TextField tf2 = new TextField();
          tf2.setPromptText("Enter duration in seconds");
          tf2.setPrefColumnCount(25);
        
        Button btn = new Button();
        btn.setText("Calculate Storage");
        
        btn.setOnAction(new EventHandler<ActionEvent>()
        {
            
            @Override
            public void handle(ActionEvent event)
                {   vid.setDurationInSeconds(Double.parseDouble(tf2.getText()));
                    vid.setStorage(Integer.parseInt(tf1.getText()));
                labe.setText("Required storage is " + vid.getStorage() + "MB  "+ " and  " +
                        vid.getStorageInGB()+ " GB" );// response
                
                }
        });
        
        FlowPane root = new FlowPane(10, 10);
        
        Separator seperator = new Separator();
        seperator.setPrefWidth(180);
        root.setAlignment(Pos.TOP_LEFT);
        root.getChildren().add(btn);
        root.getChildren().addAll(tf1, tf2, labe);
        
        
        
        
        Scene scene = new Scene(root, 400, 400);
        
        primaryStage.setTitle("Video Storage Calculator");
        primaryStage.setScene(scene);
        primaryStage.show();
        }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
        {
        launch(args);
        }
    
}
