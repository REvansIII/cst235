/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VideoEnterpriseJavaBean;

import javax.ejb.Stateless;

/**
 *
 * @author Robbie
 */
@Stateless
public class VideoEJB implements VideoEJBRemote
{

    private double durationInSeconds;
    private int videoQuality;
    private double storage;
    private final double storageInGB = storage / 1000;
    private final double bitrate720_MB_perSec = 0.333; //(20MB/min) / 60
    private final double bitrate1080_MB_perSec = 0.583; //(35MB/min) / 60
    
    
    public VideoEJB(){}
    
    @Override
        public void setStorage(int videoQuality)
        {
        switch(videoQuality)
        {
            case 1080:
                storage = getDurationInSeconds() * bitrate1080_MB_perSec;
                break;
            case 720:
                storage = getDurationInSeconds() * bitrate720_MB_perSec;
                break;
            default: 
                System.out.println("Invalid entry");
                break;
        }
        }

    @Override
    public double getStorage()
        {
            return storage;
        }

    @Override
    public double getStorageInGB()
        {
            return storage / 1000;  
        }

    @Override
    public void setDurationInSeconds(double durationInSeconds)
        {
            this.durationInSeconds = durationInSeconds; 
        }

    @Override
    public double getDurationInSeconds()
        {
            return durationInSeconds;      
        }

  /*  @Override
    public void setVideoQuality(int videoQuality)
        {
            this.videoQuality = videoQuality;
        }

    @Override
    public int getVideoQuality()
        {
          return videoQuality;
        } 
*/

   
}
