/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VideoEnterpriseJavaBean;

import javax.ejb.Remote;

/**
 *
 * @author Robbie
 */
@Remote
public interface VideoEJBRemote
{
   void setStorage(int videoQuality);
   double getStorage();
   double getStorageInGB();
   void setDurationInSeconds(double durationInSeconds);
   double getDurationInSeconds();
 //  void setVideoQuality(double videoQuality);
 //  double getVideoQuality();
    
}
