25Feb2018
A JavaFx application was created that utilizes the bean from Topic 1 short assignment 1.  The application calculates the necessary memory for streaming
video based on the quality of the video, 1080p or 720p, and duration of the video in seconds.  Input was obtained via 2 text fields and the action event was 
taken care of by way of a button.
