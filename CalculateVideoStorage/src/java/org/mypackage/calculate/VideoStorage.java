/*
 *.
 */
package org.mypackage.calculate;

/**
 *
 * @author Robbie
 */
public class VideoStorage {
    
     private int durationInSeconds;
     private int videoQuality;
    private double storage;
    private final double storageInGB = storage / 1000;
    private final double bitrate720_MB_perSec = 0.333; //(20MB/min) / 60
    private final double bitrate1080_MB_perSec = 0.583; //(35MB/min) / 60
    
    public VideoStorage(){}

    

    /**
     * @return the storage
     */
    public double getStorage() {
        return storage;
    }

    /**
     * @param videoQuality
     */
    public void setStorage(int videoQuality) 
    {
        if(videoQuality == 1080)
        {   
            storage = getDurationInSeconds() * bitrate1080_MB_perSec;
        } else if (videoQuality == 720)
          {
              storage = getDurationInSeconds() * bitrate720_MB_perSec;
          }
        else {System.out.println("Invalid entry");}
    }

    /**
     * @return the storageInGB
     */
    public double getStorageInGB() {
        return storageInGB;
    }

    /**
     * @return the durationInSeconds
     */
    public int getDurationInSeconds() {
        return durationInSeconds;
    }

    /**
     * @param durationInSeconds the durationInSeconds to set
     */
    public void setDurationInSeconds(int durationInSeconds) {
        this.durationInSeconds = durationInSeconds;
    }

    /**
     * @return the videoQuality
     */
    public int getVideoQuality() {
        return videoQuality;
    }

    /**
     * @param videoQuality the videoQuality to set
     */
    public void setVideoQuality(int videoQuality) {
        this.videoQuality = videoQuality;
    }

   
    
  

    
}
