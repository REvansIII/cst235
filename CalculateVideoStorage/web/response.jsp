<%-- 
    Document   : response
    Created on : Feb 15, 2018, 11:40:08 PM
    Author     : Robbie
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Solution Page</title>
    </head>
    <body>
        <jsp:useBean id="mybean" scope="application" class="org.mypackage.calculate.VideoStorage" />
        <jsp:setProperty name="mybean" property="videoQuality" />
        <jsp:setProperty name="mybean" property="durationInSeconds" /> 
        
        
        <h1>Storage required is <jsp:getProperty name="mybean" property="storage" /> in MB and
            <jsp:getProperty name="mybean" property="storageInGB" /> in GB 
        </h1>
    </body>
</html>
